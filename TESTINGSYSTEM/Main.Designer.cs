﻿namespace TESTINGSYSTEM
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_CreateAccount = new System.Windows.Forms.Button();
            this.btn_Transaction = new System.Windows.Forms.Button();
            this.lbl_Main = new System.Windows.Forms.Label();
            this.btn_transfer = new System.Windows.Forms.Button();
            this.btn_logout = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_CreateAccount
            // 
            this.btn_CreateAccount.Location = new System.Drawing.Point(217, 129);
            this.btn_CreateAccount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_CreateAccount.Name = "btn_CreateAccount";
            this.btn_CreateAccount.Size = new System.Drawing.Size(172, 62);
            this.btn_CreateAccount.TabIndex = 0;
            this.btn_CreateAccount.Text = "Create Bank Account";
            this.btn_CreateAccount.UseVisualStyleBackColor = true;
            this.btn_CreateAccount.Click += new System.EventHandler(this.btn_CreateAccount_Click);
            // 
            // btn_Transaction
            // 
            this.btn_Transaction.Location = new System.Drawing.Point(217, 217);
            this.btn_Transaction.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Transaction.Name = "btn_Transaction";
            this.btn_Transaction.Size = new System.Drawing.Size(173, 60);
            this.btn_Transaction.TabIndex = 1;
            this.btn_Transaction.Text = "Deposit";
            this.btn_Transaction.UseVisualStyleBackColor = true;
            this.btn_Transaction.Click += new System.EventHandler(this.btn_Transaction_Click);
            // 
            // lbl_Main
            // 
            this.lbl_Main.AutoSize = true;
            this.lbl_Main.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Main.Location = new System.Drawing.Point(123, 53);
            this.lbl_Main.Name = "lbl_Main";
            this.lbl_Main.Size = new System.Drawing.Size(344, 25);
            this.lbl_Main.TabIndex = 2;
            this.lbl_Main.Text = "Welcome to Bank Account System";
            // 
            // btn_transfer
            // 
            this.btn_transfer.Location = new System.Drawing.Point(217, 304);
            this.btn_transfer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_transfer.Name = "btn_transfer";
            this.btn_transfer.Size = new System.Drawing.Size(173, 60);
            this.btn_transfer.TabIndex = 3;
            this.btn_transfer.Text = "Transfer";
            this.btn_transfer.UseVisualStyleBackColor = true;
            this.btn_transfer.Click += new System.EventHandler(this.btn_transfer_Click);
            // 
            // btn_logout
            // 
            this.btn_logout.Location = new System.Drawing.Point(217, 395);
            this.btn_logout.Name = "btn_logout";
            this.btn_logout.Size = new System.Drawing.Size(172, 60);
            this.btn_logout.TabIndex = 4;
            this.btn_logout.Text = "Log out";
            this.btn_logout.UseVisualStyleBackColor = true;
            this.btn_logout.Click += new System.EventHandler(this.btn_logout_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(576, 526);
            this.Controls.Add(this.btn_logout);
            this.Controls.Add(this.btn_transfer);
            this.Controls.Add(this.lbl_Main);
            this.Controls.Add(this.btn_Transaction);
            this.Controls.Add(this.btn_CreateAccount);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main";
            this.Load += new System.EventHandler(this.Main_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_CreateAccount;
        private System.Windows.Forms.Button btn_Transaction;
        private System.Windows.Forms.Label lbl_Main;
        private System.Windows.Forms.Button btn_transfer;
        private System.Windows.Forms.Button btn_logout;
    }
}