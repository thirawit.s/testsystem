﻿using BankAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BankAPI.Services
{
    public class CustomerServices
    {
        public static void CreateAccount(Model.CustomerModel model)
        {
            
            string query = "Insert into Customer values(@FirstName,@LastName,GETDATE(),'Y')";
            string conn = System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
            using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(conn))
            {
                System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(query, connection);
                command.Parameters.AddWithValue("@FirstName", model.FirstName);
                command.Parameters.AddWithValue("@LastName", model.LastName);
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    
                    
                }
                catch(Exception ex)
                {
                  
                }
                finally
                {
                    connection.Close();
                  
                }
            }

            
        }

        public static string GetCustomerID(Model.CustomerModel model)
        {
            Model.ApiResult apiResult = new Model.ApiResult();
            string id = string.Empty;
            string query = "select Customer_Id from Customer where Customer_FirstName = @FirstName and Customer_LastName = @LastName";
            string conn = System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
            using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(conn))
            {
                System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(query, connection);
                command.Parameters.AddWithValue("@FirstName", model.FirstName);
                command.Parameters.AddWithValue("@LastName", model.LastName);
                connection.Open();
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();

                try
                {
                    da.SelectCommand = command;
                    System.Data.DataSet ds = new System.Data.DataSet();
                    ///conn.Open();
                    da.Fill(ds);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        id = ds.Tables[0].Rows[0]["Customer_Id"].ToString();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    connection.Close();

                }
            }

            return id;
        }

        public static bool CheckBalance(Model.TransactionModel model)
        {
            bool enoughbalance = false;
            Model.ApiResult apiResult = new Model.ApiResult();
            string query = "select Account_Balance from Account  where Account_IBANNo = @IBAN ";
            string conn = System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
            using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(conn))
            {
                System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(query, connection);
                command.Parameters.AddWithValue("@IBAN", model.IBAN);
 
                connection.Open();
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();

                try
                {
                    da.SelectCommand = command;
                    System.Data.DataSet ds = new System.Data.DataSet();
                    ///conn.Open();
                    da.Fill(ds);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string total = ds.Tables[0].Rows[0]["Account_Balance"].ToString();
                        Decimal balance = Decimal.Parse(total);
                        if(balance > model.Total)
                        {
                            enoughbalance = true;
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    connection.Close();

                }
            }

            return enoughbalance;
        }

        public static Model.ApiResult Transaction(Model.TransactionModel model)
        {
            Model.ApiResult apiResult = new ApiResult();
            string query = string.Empty;
            bool balacecheck = false;

            if (model.TYPE == "TRANSFER")
            {
                balacecheck = CheckBalance(model);
                query = "INSERT INTO TRANSACTIONS VALUES(@TYPE,@SOURCE,@TO,@AMOUNT,0,@TOTAL,GETDATE())";
            }
            else
            {
                query = "INSERT INTO TRANSACTIONS VALUES(@TYPE,null,@TO,@AMOUNT,@FEE,@TOTAL,GETDATE())";

            }

            if (model.TYPE == "DEPOSIT" || model.TYPE == "TRANSFER" && balacecheck == true)
            {

                string conn = System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
                using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(conn))
                {
                    System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();
                    System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(query, connection);
                    command.Parameters.AddWithValue("@TYPE", model.TYPE);
                    if (model.TYPE == "TRANSFER")
                    {
                        command.Parameters.AddWithValue("@SOURCE", model.IBAN);
                        command.Parameters.AddWithValue("@TO", model.TO);
                        command.Parameters.AddWithValue("@AMOUNT", model.Amount);
                        command.Parameters.AddWithValue("@TOTAL", model.Total);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@TO", model.TO);
                        command.Parameters.AddWithValue("@AMOUNT", model.Amount);
                        command.Parameters.AddWithValue("@FEE", model.Fee);
                        command.Parameters.AddWithValue("@TOTAL", model.Total);
                    }

                    connection.Open();
                    try
                    {
                        command.ExecuteNonQuery();
                        apiResult.ResponseCode = "00";
                        if (model.TYPE == "TRANSFER")
                        {
                            ProcessMinusBalanceTransaction(model);
                            ProcessAddBalanceTransaction(model);
                            
                        }
                        else
                        {
                            ProcessAddBalanceTransaction(model);
                        }
                    }
                    catch (Exception ex)
                    {
                        apiResult.ResponseCode = "69";
                    }
                    finally
                    {
                        connection.Close();

                    }
                }
            }
            else
            {
                if(balacecheck == false)
                {
                    apiResult.ResponseCode = "02";
                }
                else
                {
                    apiResult.ResponseCode = "69";
                }
               
            }

            return apiResult;
        }

        public static void ProcessAddBalanceTransaction(Model.TransactionModel model)
        {
            string query = "Update Account SET Account_Balance = Account_Balance + @TOTAL where Account_IBANNo = @TO ";
            string conn = System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
            using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(conn))
            {
                System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(query, connection);
                command.Parameters.AddWithValue("@TOTAL", model.Total);
                command.Parameters.AddWithValue("@TO", model.TO);
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();


                }
                catch (Exception ex)
                {

                }
                finally
                {
                    connection.Close();

                }
            }
        }

        public static void ProcessMinusBalanceTransaction(Model.TransactionModel model)
        {
            string query = "Update Account SET Account_Balance = Account_Balance - @TOTAL where Account_IBANNo = @SOURCE ";
            string conn = System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
            using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(conn))
            {
                System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(query, connection);
                command.Parameters.AddWithValue("@TOTAL", model.Total);
                command.Parameters.AddWithValue("@SOURCE", model.IBAN);
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();


                }
                catch (Exception ex)
                {

                }
                finally
                {
                    connection.Close();

                }
            }
        }
        public static Model.ApiResult CreateBankAccount(Model.CustomerModel model)
        {
            Model.ApiResult apiResult = new ApiResult();
            string Id = GetCustomerID(model);
            string query = "Insert into Account values(@IBAN,GETDATE(),'Y',@ID,@BBAN,@BANKNAME,0)";
            string conn = System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
            using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(conn))
            {
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();
                System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(query, connection);
                command.Parameters.AddWithValue("@IBAN", model.IBAN);
                command.Parameters.AddWithValue("@ID", Id);
                command.Parameters.AddWithValue("@BBAN", model.BankAccountNo);
                command.Parameters.AddWithValue("@BANKNAME", model.BankName);
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    if(CheckUserExits(Id) == false)
                    {
                        CreateUserForCustomers(Id);
                    }

                    apiResult = GetUsers(Id);

                    apiResult.ResponseCode = "00";
                }
                catch (Exception ex)
                {
                    apiResult.ResponseCode = "69";
                }
                finally
                {
                    connection.Close();

                }
            }

            return apiResult;
        }

        public static void CreateUserForCustomers(string Id)
        {
            Model.ApiResult apiResult = new Model.ApiResult();
            string query = "Insert into Users values(@USER,@PASSWORD,'2',GETDATE(),'Y',@ID)";
            string conn = System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
            using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(conn))
            {
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();
                System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(query, connection);
                command.Parameters.AddWithValue("@USER","USER"+Id);
                command.Parameters.AddWithValue("@PASSWORD", "PASSWORD"+Id);
                command.Parameters.AddWithValue("@ID", Id);
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    connection.Close();

                }
            }
        }

        public static Model.ApiResult GetUsers(string Id)
        {
            Model.ApiResult apiResult = new Model.ApiResult();
            string query = " select * from users where user_customerid = @Id ";
            string conn = System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
            using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(conn))
            {
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();
                System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(query, connection);
                command.Parameters.AddWithValue("@Id", Id);
                connection.Open();
                try
                {
                    da.SelectCommand = command;
                    System.Data.DataSet ds = new System.Data.DataSet();
                    ///conn.Open();
                    da.Fill(ds);

                    CustomerModel data = new CustomerModel();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        data.UserName = ds.Tables[0].Rows[0]["User_Id"].ToString();
                        data.Password = ds.Tables[0].Rows[0]["User_Password"].ToString();
                       
                        apiResult.ResultModel = data;
                    }
                    
                    
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    connection.Close();

                }
            }

            return apiResult;
        }

        public static Model.ApiResult GetIBBANNO(CustomerModel data)
        {
            Model.ApiResult apiResult = new Model.ApiResult();
            string query = " select * from Account  where Account_Customer_Id = @Id ";
            string conn = System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
            using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(conn))
            {
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();
                System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(query, connection);
                command.Parameters.AddWithValue("@Id", data.Id);
                connection.Open();
                try
                {
                    da.SelectCommand = command;
                    System.Data.DataSet ds = new System.Data.DataSet();
                    da.Fill(ds);
                    CustomerModel res = new CustomerModel();
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        res.IBAN = ds.Tables[0].Rows[0]["Account_IBANNo"].ToString();
                        apiResult.ResultModel = res;
                        apiResult.ResponseCode = "00";
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    connection.Close();

                }
            }

            return apiResult;
        }

        public static bool CheckUserExits(string Id)
        {
            bool found = false;
            string query = " select * from users where users_customerid = @Id ";
            string conn = System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
            using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(conn))
            {
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();
                System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(query, connection);
                command.Parameters.AddWithValue("@Id", Id);
                connection.Open();
                try
                {
                    da.SelectCommand = command;
                    System.Data.DataSet ds = new System.Data.DataSet();
                    ///conn.Open();
                    da.Fill(ds);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        found = true;
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    connection.Close();

                }
            }

            return found;
        }

        public static Model.ApiResult CheckDuplicateIBAN(Model.CustomerModel model)
        {
            Model.ApiResult apiResult = new Model.ApiResult();

            string query = "select * from Account where Account_IBANNo = @IBAN ";
            string conn = System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
            using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(conn))
            {
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();
                System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(query, connection);
                command.Parameters.AddWithValue("@IBAN", model.IBAN);
                connection.Open();
                try
                {
                    da.SelectCommand = command;
                    System.Data.DataSet ds = new System.Data.DataSet();
                    ///conn.Open();
                    da.Fill(ds);
                    
                    if(ds.Tables[0].Rows.Count > 0)
                    {
                        apiResult.ResponseCode = "04";
                    }
                    else
                    {
                        apiResult.ResponseCode = "00";
                    }

                }
                catch (Exception ex)
                {
                    apiResult.ResponseCode = "69";
                }
                finally
                {
                    connection.Close();

                }
            }

            return apiResult;
        }

        public static Model.ApiResult CheckDuplicateIBANForTransfer(Model.TransactionModel model)
        {
            Model.ApiResult apiResult = new Model.ApiResult();

            if(string.IsNullOrEmpty(model.IBAN))
            {
                string query = "select * from Account where Account_IBANNo = @TO ";
                string conn = System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
                using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(conn))
                {
                    System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();
                    System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(query, connection);
                  
                    command.Parameters.AddWithValue("@TO", model.TO);
                    connection.Open();
                    try
                    {
                        da.SelectCommand = command;
                        System.Data.DataSet ds = new System.Data.DataSet();
                        ///conn.Open();
                        da.Fill(ds);

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            apiResult.ResponseCode = "04";
                        }
                        else
                        {
                            apiResult.ResponseCode = "00";
                        }

                    }
                    catch (Exception ex)
                    {
                        apiResult.ResponseCode = "69";
                    }
                    finally
                    {
                        connection.Close();

                    }
                }
            }
            else
            {
                string query = "select * from Account where Account_IBANNo in (@IBAN,@TO) ";
                string conn = System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
                using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(conn))
                {
                    System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();
                    System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(query, connection);
                    command.Parameters.AddWithValue("@IBAN", model.IBAN);
                    command.Parameters.AddWithValue("@TO", model.TO);
                    connection.Open();
                    try
                    {
                        da.SelectCommand = command;
                        System.Data.DataSet ds = new System.Data.DataSet();
                        ///conn.Open();
                        da.Fill(ds);

                        if (ds.Tables[0].Rows.Count > 1)
                        {
                            apiResult.ResponseCode = "04";
                        }
                        else
                        {
                            apiResult.ResponseCode = "00";
                        }

                    }
                    catch (Exception ex)
                    {
                        apiResult.ResponseCode = "69";
                    }
                    finally
                    {
                        connection.Close();

                    }
                }
            }
            

            return apiResult;
        }

    }
}