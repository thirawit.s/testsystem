﻿using BankAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BankAPI.Services
{
    public class LoginServices
    {
        public static Model.ApiResult UserLogin(Model.LoginModel model)
        {
            Model.ApiResult apiResult = new Model.ApiResult();

            string query = @"select * from Users u
                             join Role r on r.Id = u.User_role
                             where u.USER_ID = @USER AND u.USER_PASSWORD = @PASSWORD  and r.Role_Status = 'Y'";
            string conn = System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
            using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(conn))
            {
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();
                System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(query, connection);
                command.Parameters.AddWithValue("@USER", model.User);
                command.Parameters.AddWithValue("@PASSWORD", model.Password);
                connection.Open();
                try
                {
                    LoginModel data = new LoginModel();
                    da.SelectCommand = command;
                    System.Data.DataSet ds = new System.Data.DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        apiResult.ResponseCode = "04";
                    }
                    else
                    {
                        data.User = ds.Tables[0].Rows[0]["User_CustomerId"].ToString();
                        data.Role = ds.Tables[0].Rows[0]["ROLE_NAME"].ToString();
                        apiResult.ResultModel = data;
                        apiResult.ResponseCode = "00";
                    }
                }
                catch (Exception ex)
                {
                    apiResult.ResponseCode = "69";
                }
                finally
                {
                    connection.Close();

                }
            }

            return apiResult;
        }

        
    }
}