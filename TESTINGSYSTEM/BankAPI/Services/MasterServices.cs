﻿using BankAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BankAPI.Services
{
    public class MasterServices
    {
        public static Model.ApiResult GetBank()
        {
            Model.ApiResult apiResult = new Model.ApiResult();

            string query = @"select * from Bank where Bank_Status = 'Y' ";
            string conn = System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
            using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(conn))
            {
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();
                System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(query, connection);
 
                connection.Open();
                try
                {
                    List<BankModel> data = new List<BankModel>();
                    da.SelectCommand = command;
                    System.Data.DataSet ds = new System.Data.DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        apiResult.ResponseCode = "04";
                    }
                    else
                    {
                      

                        foreach (System.Data.DataRow row in ds.Tables[0].Rows)
                        {
                            BankModel model = new BankModel();
                            model.Bank_Name = row["Bank_Name"].ToString();
                            data.Add(model);
                        }
                        apiResult.ResultModel = data;
                        apiResult.ResponseCode = "00";
                    }
                }
                catch (Exception ex)
                {
                    apiResult.ResponseCode = "69";
                }
                finally
                {
                    connection.Close();

                }
            }

            return apiResult;
        }
    }
}