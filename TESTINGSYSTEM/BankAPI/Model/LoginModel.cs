﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BankAPI.Model
{
    public class LoginModel
    {
        public string User { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }
}