﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankAPI.Model
{
    public class TransactionModel
    {
        public string IBAN { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Fee { get; set; }
        public decimal? Total { get; set; }
        public string TO { get; set; }
        public string TYPE { get; set; }
    }
}
