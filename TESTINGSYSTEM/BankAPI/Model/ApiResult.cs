﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BankAPI.Model
{
    public  class ApiResult
    {
        public  string ResponseCode { get; set; }
        public  object ResultModel { get; set; }
    }
}