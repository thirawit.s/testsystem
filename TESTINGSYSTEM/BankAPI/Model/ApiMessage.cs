﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BankAPI.Services
{
    public class ApiMessage
    {
        public const string Success = "00";
        public const string UnhandleException = "69";
        public const string NotFoundBankAccount = "04";
    }
}