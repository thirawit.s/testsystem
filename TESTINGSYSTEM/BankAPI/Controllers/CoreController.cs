﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BankAPI.Controllers
{
    public class CoreController : ApiController
    {
        [Route("api/Core/CreateAccount")]
        [HttpPost]
        public IHttpActionResult CreateAccount(Model.CustomerModel data)
        {
            Model.ApiResult res = new Model.ApiResult();
            Services.CustomerServices.CreateAccount(data);
            res = Services.CustomerServices.CreateBankAccount(data);
            return Ok(res);
        }

        [Route("api/Core/Transaction")]
        [HttpPost]
        public IHttpActionResult Transaction(Model.TransactionModel data)
        {
            Model.ApiResult res = new Model.ApiResult();
            res = Services.CustomerServices.Transaction(data);
            return Ok(res);
        }
        
       

        [Route("api/Core/CheckExistIBANTransfer")]
        [HttpPost]
        public IHttpActionResult CheckExistIBANTransfer(Model.TransactionModel data)
        {
            Model.ApiResult res = new Model.ApiResult();
            res = Services.CustomerServices.CheckDuplicateIBANForTransfer(data);
            return Ok(res);
        }


        [Route("api/Core/CheckDuplicateIBAN")]
        [HttpPost]
        public IHttpActionResult CheckDuplicateIBAN(Model.CustomerModel data)
        {
            Model.ApiResult res = new Model.ApiResult();
            res = Services.CustomerServices.CheckDuplicateIBAN(data);
            return Ok(res);
        }
        [Route("api/Core/Login")]
        [HttpPost]
        public IHttpActionResult UserLogin(Model.LoginModel data)
        {
            Model.ApiResult res = new Model.ApiResult();
            res = Services.LoginServices.UserLogin(data);
            return Ok(res);
        }

        [Route("api/Core/GetIBANNO")]
        [HttpPost]
        public IHttpActionResult GetIBANNO(Model.CustomerModel data)
        {
            Model.ApiResult res = new Model.ApiResult();
            res = Services.CustomerServices.GetIBBANNO(data);
            return Ok(res);
        }

        

        [Route("api/Core/GetBank")]
        [HttpGet]
        public IHttpActionResult GetBank()
        {
            Model.ApiResult res = new Model.ApiResult();
            res = Services.MasterServices.GetBank();
            return Ok(res);
        }


    }
}
