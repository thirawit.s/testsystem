﻿namespace TESTINGSYSTEM
{
    partial class Transaction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gb_Deposit = new System.Windows.Forms.GroupBox();
            this.btn_deposit = new System.Windows.Forms.Button();
            this.txt_Amount = new System.Windows.Forms.TextBox();
            this.lbl_Amount = new System.Windows.Forms.Label();
            this.txt_IBAN = new System.Windows.Forms.TextBox();
            this.lbl_IBANSource = new System.Windows.Forms.Label();
            this.gb_Transfer = new System.Windows.Forms.GroupBox();
            this.btn_Transfer = new System.Windows.Forms.Button();
            this.txt_TransferAmount = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_Tranfer_SOURCE = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_To = new System.Windows.Forms.TextBox();
            this.gb_Deposit.SuspendLayout();
            this.gb_Transfer.SuspendLayout();
            this.SuspendLayout();
            // 
            // gb_Deposit
            // 
            this.gb_Deposit.Controls.Add(this.btn_deposit);
            this.gb_Deposit.Controls.Add(this.txt_Amount);
            this.gb_Deposit.Controls.Add(this.lbl_Amount);
            this.gb_Deposit.Controls.Add(this.txt_IBAN);
            this.gb_Deposit.Controls.Add(this.lbl_IBANSource);
            this.gb_Deposit.Location = new System.Drawing.Point(83, 33);
            this.gb_Deposit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gb_Deposit.Name = "gb_Deposit";
            this.gb_Deposit.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gb_Deposit.Size = new System.Drawing.Size(649, 259);
            this.gb_Deposit.TabIndex = 0;
            this.gb_Deposit.TabStop = false;
            this.gb_Deposit.Text = "Transaction : Deposit";
            // 
            // btn_deposit
            // 
            this.btn_deposit.Location = new System.Drawing.Point(220, 197);
            this.btn_deposit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_deposit.Name = "btn_deposit";
            this.btn_deposit.Size = new System.Drawing.Size(111, 41);
            this.btn_deposit.TabIndex = 5;
            this.btn_deposit.Text = "Deposit";
            this.btn_deposit.UseVisualStyleBackColor = true;
            this.btn_deposit.Click += new System.EventHandler(this.btn_deposit_Click);
            // 
            // txt_Amount
            // 
            this.txt_Amount.Location = new System.Drawing.Point(220, 142);
            this.txt_Amount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_Amount.Name = "txt_Amount";
            this.txt_Amount.Size = new System.Drawing.Size(180, 22);
            this.txt_Amount.TabIndex = 4;
             // 
            // lbl_Amount
            // 
            this.lbl_Amount.AutoSize = true;
            this.lbl_Amount.Location = new System.Drawing.Point(5, 145);
            this.lbl_Amount.Name = "lbl_Amount";
            this.lbl_Amount.Size = new System.Drawing.Size(189, 17);
            this.lbl_Amount.TabIndex = 3;
            this.lbl_Amount.Text = "Amount (* Fee Charge 0.1%)";
            // 
            // txt_IBAN
            // 
            this.txt_IBAN.Location = new System.Drawing.Point(220, 80);
            this.txt_IBAN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_IBAN.Name = "txt_IBAN";
            this.txt_IBAN.Size = new System.Drawing.Size(180, 22);
            this.txt_IBAN.TabIndex = 2;
             // 
            // lbl_IBANSource
            // 
            this.lbl_IBANSource.AutoSize = true;
            this.lbl_IBANSource.Location = new System.Drawing.Point(67, 80);
            this.lbl_IBANSource.Name = "lbl_IBANSource";
            this.lbl_IBANSource.Size = new System.Drawing.Size(39, 17);
            this.lbl_IBANSource.TabIndex = 1;
            this.lbl_IBANSource.Text = "IBAN";
            // 
            // gb_Transfer
            // 
            this.gb_Transfer.Controls.Add(this.txt_To);
            this.gb_Transfer.Controls.Add(this.label3);
            this.gb_Transfer.Controls.Add(this.btn_Transfer);
            this.gb_Transfer.Controls.Add(this.txt_TransferAmount);
            this.gb_Transfer.Controls.Add(this.label1);
            this.gb_Transfer.Controls.Add(this.txt_Tranfer_SOURCE);
            this.gb_Transfer.Controls.Add(this.label2);
            this.gb_Transfer.Location = new System.Drawing.Point(77, 34);
            this.gb_Transfer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gb_Transfer.Name = "gb_Transfer";
            this.gb_Transfer.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gb_Transfer.Size = new System.Drawing.Size(649, 258);
            this.gb_Transfer.TabIndex = 6;
            this.gb_Transfer.TabStop = false;
            this.gb_Transfer.Text = "Transaction : Transfer";
            // 
            // btn_Transfer
            // 
            this.btn_Transfer.Location = new System.Drawing.Point(220, 186);
            this.btn_Transfer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Transfer.Name = "btn_Transfer";
            this.btn_Transfer.Size = new System.Drawing.Size(111, 41);
            this.btn_Transfer.TabIndex = 5;
            this.btn_Transfer.Text = "Transfer";
            this.btn_Transfer.UseVisualStyleBackColor = true;
            this.btn_Transfer.Click += new System.EventHandler(this.btn_Transfer_Click);
            // 
            // txt_TransferAmount
            // 
            this.txt_TransferAmount.Location = new System.Drawing.Point(220, 130);
            this.txt_TransferAmount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_TransferAmount.Name = "txt_TransferAmount";
            this.txt_TransferAmount.Size = new System.Drawing.Size(180, 22);
            this.txt_TransferAmount.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(67, 135);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Amount ";
            // 
            // txt_Tranfer_SOURCE
            // 
            this.txt_Tranfer_SOURCE.Location = new System.Drawing.Point(220, 43);
            this.txt_Tranfer_SOURCE.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_Tranfer_SOURCE.Name = "txt_Tranfer_SOURCE";
            this.txt_Tranfer_SOURCE.Size = new System.Drawing.Size(180, 22);
            this.txt_Tranfer_SOURCE.TabIndex = 2;
             // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(67, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "IBAN_SOURCE";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(67, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "IBAN_TO";
            // 
            // txt_To
            // 
            this.txt_To.Location = new System.Drawing.Point(220, 83);
            this.txt_To.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_To.Name = "txt_To";
            this.txt_To.Size = new System.Drawing.Size(180, 22);
            this.txt_To.TabIndex = 7;
             // 
            // Transaction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 393);
            this.Controls.Add(this.gb_Transfer);
            this.Controls.Add(this.gb_Deposit);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Transaction";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Transaction";
            this.gb_Deposit.ResumeLayout(false);
            this.gb_Deposit.PerformLayout();
            this.gb_Transfer.ResumeLayout(false);
            this.gb_Transfer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_Deposit;
        private System.Windows.Forms.Label lbl_IBANSource;
        private System.Windows.Forms.Button btn_deposit;
        private System.Windows.Forms.TextBox txt_Amount;
        private System.Windows.Forms.Label lbl_Amount;
        private System.Windows.Forms.TextBox txt_IBAN;
        private System.Windows.Forms.GroupBox gb_Transfer;
        private System.Windows.Forms.TextBox txt_To;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_Transfer;
        private System.Windows.Forms.TextBox txt_TransferAmount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_Tranfer_SOURCE;
        private System.Windows.Forms.Label label2;
    }
}