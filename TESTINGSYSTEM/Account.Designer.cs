﻿namespace TESTINGSYSTEM
{
    partial class Account
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_generate = new System.Windows.Forms.Button();
            this.ddl_Bank = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_IBAN = new System.Windows.Forms.TextBox();
            this.lbl_Bank = new System.Windows.Forms.Label();
            this.btn_Clear = new System.Windows.Forms.Button();
            this.btn_submit = new System.Windows.Forms.Button();
            this.txt_LastName = new System.Windows.Forms.TextBox();
            this.txt_FirstName = new System.Windows.Forms.TextBox();
            this.lbl_LastName = new System.Windows.Forms.Label();
            this.lbl_FirstName = new System.Windows.Forms.Label();
            this.lbl_BBAN = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbl_BBAN);
            this.groupBox1.Controls.Add(this.btn_generate);
            this.groupBox1.Controls.Add(this.ddl_Bank);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txt_IBAN);
            this.groupBox1.Controls.Add(this.lbl_Bank);
            this.groupBox1.Controls.Add(this.btn_Clear);
            this.groupBox1.Controls.Add(this.btn_submit);
            this.groupBox1.Controls.Add(this.txt_LastName);
            this.groupBox1.Controls.Add(this.txt_FirstName);
            this.groupBox1.Controls.Add(this.lbl_LastName);
            this.groupBox1.Controls.Add(this.lbl_FirstName);
            this.groupBox1.Location = new System.Drawing.Point(117, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(639, 347);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Customer_Info";
            // 
            // btn_generate
            // 
            this.btn_generate.Location = new System.Drawing.Point(333, 211);
            this.btn_generate.Name = "btn_generate";
            this.btn_generate.Size = new System.Drawing.Size(101, 31);
            this.btn_generate.TabIndex = 16;
            this.btn_generate.Text = "Generate IBAN";
            this.btn_generate.UseVisualStyleBackColor = true;
            this.btn_generate.Click += new System.EventHandler(this.btn_generate_Click);
            // 
            // ddl_Bank
            // 
            this.ddl_Bank.FormattingEnabled = true;
            this.ddl_Bank.Location = new System.Drawing.Point(119, 160);
            this.ddl_Bank.Name = "ddl_Bank";
            this.ddl_Bank.Size = new System.Drawing.Size(201, 24);
            this.ddl_Bank.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 221);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 17);
            this.label1.TabIndex = 14;
            this.label1.Text = "IBAN";
            // 
            // txt_IBAN
            // 
            this.txt_IBAN.Location = new System.Drawing.Point(119, 215);
            this.txt_IBAN.Name = "txt_IBAN";
            this.txt_IBAN.ReadOnly = true;
            this.txt_IBAN.Size = new System.Drawing.Size(201, 22);
            this.txt_IBAN.TabIndex = 13;
            // 
            // lbl_Bank
            // 
            this.lbl_Bank.AutoSize = true;
            this.lbl_Bank.Location = new System.Drawing.Point(25, 163);
            this.lbl_Bank.Name = "lbl_Bank";
            this.lbl_Bank.Size = new System.Drawing.Size(45, 17);
            this.lbl_Bank.TabIndex = 12;
            this.lbl_Bank.Text = "BANK";
            // 
            // btn_Clear
            // 
            this.btn_Clear.Location = new System.Drawing.Point(333, 282);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(158, 41);
            this.btn_Clear.TabIndex = 11;
            this.btn_Clear.Text = "Clear";
            this.btn_Clear.UseVisualStyleBackColor = true;
            this.btn_Clear.Click += new System.EventHandler(this.btn_Clear_Click);
            // 
            // btn_submit
            // 
            this.btn_submit.Location = new System.Drawing.Point(119, 282);
            this.btn_submit.Name = "btn_submit";
            this.btn_submit.Size = new System.Drawing.Size(158, 41);
            this.btn_submit.TabIndex = 10;
            this.btn_submit.Text = "Create Account";
            this.btn_submit.UseVisualStyleBackColor = true;
            this.btn_submit.Click += new System.EventHandler(this.btn_submit_Click);
            // 
            // txt_LastName
            // 
            this.txt_LastName.Location = new System.Drawing.Point(118, 102);
            this.txt_LastName.Name = "txt_LastName";
            this.txt_LastName.Size = new System.Drawing.Size(202, 22);
            this.txt_LastName.TabIndex = 3;
            this.txt_LastName.TextChanged += new System.EventHandler(this.txt_LastName_TextChanged);
            this.txt_LastName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_LastName_KeyPress);
            // 
            // txt_FirstName
            // 
            this.txt_FirstName.Location = new System.Drawing.Point(119, 52);
            this.txt_FirstName.Name = "txt_FirstName";
            this.txt_FirstName.Size = new System.Drawing.Size(201, 22);
            this.txt_FirstName.TabIndex = 2;
            this.txt_FirstName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_FirstName_KeyPress);
            // 
            // lbl_LastName
            // 
            this.lbl_LastName.AutoSize = true;
            this.lbl_LastName.Location = new System.Drawing.Point(25, 102);
            this.lbl_LastName.Name = "lbl_LastName";
            this.lbl_LastName.Size = new System.Drawing.Size(72, 17);
            this.lbl_LastName.TabIndex = 1;
            this.lbl_LastName.Text = "LastName";
            // 
            // lbl_FirstName
            // 
            this.lbl_FirstName.AutoSize = true;
            this.lbl_FirstName.Location = new System.Drawing.Point(25, 52);
            this.lbl_FirstName.Name = "lbl_FirstName";
            this.lbl_FirstName.Size = new System.Drawing.Size(72, 17);
            this.lbl_FirstName.TabIndex = 0;
            this.lbl_FirstName.Text = "FirstName";
            // 
            // lbl_BBAN
            // 
            this.lbl_BBAN.AutoSize = true;
            this.lbl_BBAN.Location = new System.Drawing.Point(116, 251);
            this.lbl_BBAN.Name = "lbl_BBAN";
            this.lbl_BBAN.Size = new System.Drawing.Size(45, 17);
            this.lbl_BBAN.TabIndex = 17;
            this.lbl_BBAN.Text = "BBAN";
            this.lbl_BBAN.Visible = false;
            // 
            // Account
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 462);
            this.Controls.Add(this.groupBox1);
            this.Name = "Account";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Account";
            this.Load += new System.EventHandler(this.Account_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.Button btn_submit;
        private System.Windows.Forms.TextBox txt_LastName;
        private System.Windows.Forms.TextBox txt_FirstName;
        private System.Windows.Forms.Label lbl_LastName;
        private System.Windows.Forms.Label lbl_FirstName;
        private System.Windows.Forms.TextBox txt_IBAN;
        private System.Windows.Forms.Label lbl_Bank;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ddl_Bank;
        private System.Windows.Forms.Button btn_generate;
        private System.Windows.Forms.Label lbl_BBAN;
    }
}

