﻿using IbanNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using TESTINGSYSTEM.DAO;
using TESTINGSYSTEM.DTO;
using TESTINGSYSTEM.Services;

namespace TESTINGSYSTEM
{
    public partial class Account : Form
    {
        public Account()
        {
            InitializeComponent();
           
        }

        private void txt_LastName_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_Clear_Click(object sender, EventArgs e)
        {
            txt_FirstName.Text = null;
            txt_LastName.Text = null;
            txt_IBAN.Text = null;
 
        }

        public void GenerateIban()
        {
            Random rnd = new Random();
            int ChecksumFirstPlace = rnd.Next(0, 9);
            int ChecksumSecondplace = rnd.Next(0, 9);
            string RandomNL = ChecksumFirstPlace.ToString() + ChecksumSecondplace.ToString();
            string BankName = ddl_Bank.SelectedValue.ToString();
            var obfuscated = $"NL"+ RandomNL+ BankName+ "XXXXXXX062";
            var IsBank = obfuscated.Contains(BankName);
            var spliting = "NL" + RandomNL + BankName;
            for (int i = 0; i < (IsBank ? 999 : 999999); i++)
            {
                var iban = obfuscated.Replace("XXXXXXX", i.ToString().PadLeft(7, '0'));
                var validationResult = Iban.Validator.Validate(iban);
                if (validationResult.IsValid)
                {
                    txt_IBAN.Text = iban.ToString();
                    string split = iban.Replace(spliting, "");
                    lbl_BBAN.Text = split;
                    return;
                }
            }

         
        }

        private void btn_submit_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            string title = "Info";
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            if (string.IsNullOrEmpty(txt_FirstName.Text))
            {
                message = "Please Fill FirstName";
                DialogResult r = MessageBox.Show(message, title, buttons);
                return;
            }
            else if(string.IsNullOrEmpty(txt_LastName.Text))
            {
                message = "Please Fill LastName";
                DialogResult r = MessageBox.Show(message, title, buttons);
                return;
            }
            else if(string.IsNullOrEmpty(txt_IBAN.Text))
            {
                message = "Please Generate IBAN";
                DialogResult r = MessageBox.Show(message, title, buttons);
                return;
            }

            CustomerModel model = new CustomerModel();
            model.FirstName = txt_FirstName.Text;
            model.LastName = txt_LastName.Text;
            model.IBAN = txt_IBAN.Text;
            model.BankAccountNo = lbl_BBAN.Text;
            model.BankName = ddl_Bank.SelectedValue.ToString();
            if(!Bank.CheckDuplicate(model))
            {
                ResponeModel<CustomerModel> res= Bank.CreateBankAccount(model);
                if(res.ResponseCode == "00")
                {
                    message = "Create Account SuccessFully username :" +res.resultModel.UserName +" and Password : " + res.resultModel.Password +"Please Noted this for use this program";
                    DialogResult result = MessageBox.Show(message, title, buttons);
                    if (result == DialogResult.OK)
                    {
                        this.Close();
                    }
                }
                else
                {
                    message = "Exception on Create Account!!";
                    DialogResult result = MessageBox.Show(message, title, buttons);
                     
                }
               

            }
            else
            {

            }
          
            
        }

        private void Account_Load(object sender, EventArgs e)
        {
            try
            {
                
                string urlapi = ConfigurationManager.AppSettings["urlapi"].ToString();
                ResponeModel<List<BankModel>> data = CallApiServices.GetDropdownDataBank(urlapi);
                if (data != null)
                {
                    ddl_Bank.DataSource = data.resultModel;
                    ddl_Bank.ValueMember = "Bank_Name";
                    ddl_Bank.DisplayMember = "Bank_Name";
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void btn_generate_Click(object sender, EventArgs e)
        {
            GenerateIban();
        }

        private void txt_FirstName_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^a-zA-Z0-9\s]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
                string message = "Cannot fill Special Character in textbox!!";
                string title = "Error";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result = MessageBox.Show(message, title, buttons);
                txt_FirstName.Text = string.Empty;
            }
        }

        private void txt_LastName_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^a-zA-Z0-9\s]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
                string message = "Cannot fill Special Character in textbox!!";
                string title = "Error";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result = MessageBox.Show(message, title, buttons);
                txt_LastName.Text = string.Empty;
            }
        }
    }
}
