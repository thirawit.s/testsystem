﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using TESTINGSYSTEM.DAO;
using TESTINGSYSTEM.DTO;

namespace TESTINGSYSTEM
{
    public partial class Transaction : Form
    {
   
        public Transaction(string Type,string user)
        {
            InitializeComponent();
            if(Type == "Deposit")
            {
                gb_Transfer.Visible = false;
            }
            else
            {   
                CustomerModel data = new CustomerModel();
                data.Id = user;
                ResponeModel<CustomerModel> res = Bank.GetFillAutoIBAN(data);
                if (res.ResponseCode == "00")
                {
                    txt_Tranfer_SOURCE.Text = res.resultModel.IBAN;
                    txt_Tranfer_SOURCE.Enabled = false;
                }
                gb_Transfer.Visible = true;
            }
        }

        private void btn_deposit_Click(object sender, EventArgs e)
        {

            if(string.IsNullOrEmpty(txt_IBAN.Text))
            {
                string message = "PLEASE FILL IBAN ACCOUNT";
                string title = "ERROR";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result = MessageBox.Show(message, title, buttons);

            }
            else if (String.IsNullOrEmpty(txt_Amount.Text))
            {
                string message = "PLEASE FILL AMOUNT";
                string title = "ERROR";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result = MessageBox.Show(message, title, buttons);

            }


            double fee = (Double.Parse(txt_Amount.Text) * 0.01);
            TransactionModel model = new TransactionModel();
            model.TO = txt_IBAN.Text;
            model.Amount =  Decimal.Parse(txt_Amount.Text);
            model.Fee  = Decimal.Parse(fee.ToString());
            model.Total = model.Amount - model.Fee;
            model.TYPE = "DEPOSIT";
            if (Bank.CheckBankAccountExits(model))
            {
                ResponeModel<TransactionModel> res = Bank.Transaction(model);
                if (res.ResponseCode == "00")
                {
                    string message = "Transaction SuccessFully";
                    string title = "Info";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult result = MessageBox.Show(message, title, buttons);
                    if (result == DialogResult.OK)
                    {
                        this.Close();
                    }
                }
            }
            else
            {
                string message = "IBAN ACCOUNT NOT FOUND  PLEASE CHECK IBAN AGAIN!!";
                string title = "ERROR";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result = MessageBox.Show(message, title, buttons);
               
            }

        }

        private void btn_Transfer_Click(object sender, EventArgs e)
        {
            if (txt_Tranfer_SOURCE.Text == txt_To.Text)
            {
                string message = "PLEASE CHECK IBAN ACCOUNT YOU CANNOT TRANSFER FOR SAME IBAN ACCOUNT !!";
                string title = "ERROR";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result = MessageBox.Show(message, title, buttons);

            }

            if (string.IsNullOrEmpty(txt_Tranfer_SOURCE.Text))
            {
                string message = "PLEASE FILL IBAN SOURCE ACCOUNT";
                string title = "ERROR";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result = MessageBox.Show(message, title, buttons);

            }
            else if (String.IsNullOrEmpty(txt_TransferAmount.Text))
            {
                string message = "PLEASE FILL AMOUNT";
                string title = "ERROR";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result = MessageBox.Show(message, title, buttons);

            }
            else if (String.IsNullOrEmpty(txt_To.Text))
            {
                string message = "PLEASE FILL  DESTINATION ACCOUNT";
                string title = "ERROR";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result = MessageBox.Show(message, title, buttons);

            }


            TransactionModel model = new TransactionModel();
            model.IBAN = txt_Tranfer_SOURCE.Text;
            model.Amount = Decimal.Parse(txt_TransferAmount.Text);
            model.TO = txt_To.Text;
            model.Total = Decimal.Parse(txt_TransferAmount.Text);
            model.TYPE = "TRANSFER";

            if (Bank.CheckBankAccountExits(model))
            {
                ResponeModel<TransactionModel> res = Bank.Transaction(model);
                if (res.ResponseCode == "00")
                {
                    string message = "Transaction Transfer to" + txt_To.Text + " From " + txt_Tranfer_SOURCE.Text + " SuccessFully";
                    string title = "Info";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult result = MessageBox.Show(message, title, buttons);
                    if (result == DialogResult.OK)
                    {
                        this.Close();
                    }
                }
                else if (res.ResponseCode == "02")
                {
                    string message = "ERROR NOT ENOUGH BALANCE";
                    string title = "Info";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult result = MessageBox.Show(message, title, buttons);
                     
                }
                else
                {
                    string message = "Transaction Transfer to" + txt_To.Text + " From " + txt_Tranfer_SOURCE.Text + " Failed  Please Check Again";
                    string title = "Info";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult result = MessageBox.Show(message, title, buttons);

                }
            }
            else
            {
                string message = "IBAN ACCOUNT NOT FOUND  PLEASE CHECK IBAN AGAIN!!";
                string title = "ERROR";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result = MessageBox.Show(message, title, buttons);

            }

        }

      
        
    }
}
