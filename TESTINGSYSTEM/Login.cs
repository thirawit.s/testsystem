﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using TESTINGSYSTEM.DAO;
using TESTINGSYSTEM.Services;

namespace TESTINGSYSTEM
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void btn_Clear_Click(object sender, EventArgs e)
        {
            txt_User.Text = string.Empty;
            txt_Password.Text = string.Empty;
        }

        private void btn_Login_Click(object sender, EventArgs e)
        {
            try
            {
                LoginModel model = new LoginModel();
                if(txt_User.Text != null && txt_User.Text != string.Empty && txt_Password.Text != null && txt_Password.Text != string.Empty)
                {
                    model.User = txt_User.Text;
                    model.Password = txt_Password.Text;
                }
                else
                {
                    string message = "Please Fill UserName and Password";
                    string title = "Error";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult result = MessageBox.Show(message, title, buttons);
                    return;
                }
                string urlapi = ConfigurationManager.AppSettings["urlapi"].ToString();
                ResponeModel<LoginModel> LoginRespone = CallApiServices.Login(urlapi, model);
                if (LoginRespone != null)
                {
                    if(LoginRespone.ResponseCode == "00")
                    {
                        model = LoginRespone.resultModel;
                        string message = "Login SuccessFully";
                        string title = "Info";
                        MessageBoxButtons buttons = MessageBoxButtons.OK;
                        DialogResult result = MessageBox.Show(message, title, buttons);
                        this.Hide();
                        var Main = new Main(model);
                        Main.Show();
                        Main.BringToFront();
                        Main.Activate();
                    }
                    else
                    {
                        string message = "User or Password is correctly please recheck again";
                        string title = "Error";
                        MessageBoxButtons buttons = MessageBoxButtons.OK;
                        DialogResult result = MessageBox.Show(message, title, buttons);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                string message = "Cannot Connect to API Please Recheck is Open";
                string title = "Error";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result = MessageBox.Show(message, title, buttons);
            }
        }

        private void txt_User_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^a-zA-Z0-9\s]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
                string message = "Cannot fill Special Character in textbox!!";
                string title = "Error";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result = MessageBox.Show(message, title, buttons);
                txt_User.Text = string.Empty;
            }
        }

        private void txt_Password_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^a-zA-Z0-9\s]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
                string message = "Cannot fill Special Character in textbox!!";
                string title = "Error";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result = MessageBox.Show(message, title, buttons);
                txt_Password.Text = string.Empty;
            }
        }

        private void btn_ano_Click(object sender, EventArgs e)
        {
            LoginModel model = new LoginModel();
            model.Role = "ANNONYMOUS";
            var Main = new Main(model);
            Main.Show();
            Main.BringToFront();
            Main.Activate();
        }
    }
}
