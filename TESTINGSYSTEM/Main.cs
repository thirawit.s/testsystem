﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TESTINGSYSTEM.DAO;
using TESTINGSYSTEM.Services;

namespace TESTINGSYSTEM
{
    public partial class Main : Form
    {
        public string user_id;
        public Main(LoginModel User)
        {
            InitializeComponent();
            if(User !=  null)
            {
                try
                {
                    if(User.Role == "ANNONYMOUS")
                    {
                        btn_CreateAccount.Visible = false;
                        btn_transfer.Visible = false;
                    }
                    else
                    {
                        user_id = User.User;
                        if (User.Role == "ADMIN")
                        {
                            btn_Transaction.Visible = false;
                            btn_transfer.Visible = false;
                        }
                        else
                        {
                            btn_CreateAccount.Visible = false;
                        }
                    }
                   
                }
                catch (Exception ex)
                {

                }
            }

        }

        private void btn_CreateAccount_Click(object sender, EventArgs e)
        {
            var Account = new Account();
            Account.Show();
            Account.BringToFront();
            Account.Activate();
        }

        private void btn_Transaction_Click(object sender, EventArgs e)
        {
            var Transaction = new Transaction("Deposit","");
            Transaction.Show();
            Transaction.BringToFront();
            Transaction.Activate();
        }

        private void Main_Load(object sender, EventArgs e)
        {

        }

        private void btn_logout_Click(object sender, EventArgs e)
        {
            this.Hide();
            var lg = new Login();
            lg.Show();
            lg.BringToFront();
            lg.Activate();
        }

        private void btn_transfer_Click(object sender, EventArgs e)
        {
            var Transaction = new Transaction("Transfer", user_id);
            Transaction.Show();
            Transaction.BringToFront();
            Transaction.Activate();
        }
    }
}
