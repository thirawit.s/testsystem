﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TESTINGSYSTEM.DAO;
using TESTINGSYSTEM.Services;

namespace TESTINGSYSTEM.DTO
{
    public class Bank
    {
        public static bool CheckDuplicate(CustomerModel model)
        {
            bool CheckDup = false;

            try
            {
                string urlapi = ConfigurationManager.AppSettings["urlapi"].ToString();
                ResponeModel<CustomerModel> cusRes = CallApiServices.CheckDuplicate(urlapi, model);
                if(cusRes != null)
                {
                    if(cusRes.ResponseCode == "04")
                    {
                        CheckDup = true;
                    }
                }
            }
            catch(Exception ex)
            {

            }
            return CheckDup;

        }

        public static bool CheckBankAccountExits(TransactionModel model)
        {
            bool Exist = false;

            try
            {
                string urlapi = ConfigurationManager.AppSettings["urlapi"].ToString();
                ResponeModel<TransactionModel> cusRes = CallApiServices.CheckExitsForTransfer(urlapi, model);
                if (cusRes != null)
                {
                    if (cusRes.ResponseCode == "04")
                    {
                        Exist = true;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Exist;

        }

        public static ResponeModel<CustomerModel> CreateBankAccount(CustomerModel model)
        {
            ResponeModel<CustomerModel> cusRes = new ResponeModel<CustomerModel>();
            try
            {
                string urlapi = ConfigurationManager.AppSettings["urlapi"].ToString();
                cusRes = CallApiServices.CreateAccount(urlapi, model);
                
            }
            catch (Exception ex)
            {

            }
            return cusRes;
        }

        public static ResponeModel<CustomerModel> GetFillAutoIBAN(CustomerModel model)
        {
            ResponeModel<CustomerModel> cusRes = new ResponeModel<CustomerModel>();
            try
            {
                string urlapi = ConfigurationManager.AppSettings["urlapi"].ToString();
                cusRes = CallApiServices.GetFillIBAN(urlapi, model);

            }
            catch (Exception ex)
            {

            }
            return cusRes;
        }
        public static ResponeModel<TransactionModel> Transaction(TransactionModel model)
        {
            ResponeModel<TransactionModel> cusRes = new ResponeModel<TransactionModel>();
            try
            {
                string urlapi = ConfigurationManager.AppSettings["urlapi"].ToString();
                cusRes = CallApiServices.Transaction(urlapi, model);

            }
            catch (Exception ex)
            {

            }
            return cusRes;
        }
    }
}
