﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TESTINGSYSTEM.DAO;

namespace TESTINGSYSTEM.Services
{
    public class CallApiServices
    {
        public static Res Request<Res, Req>(string url, Req request)
        {
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Timeout = 1000 * 60 * 3;
                httpWebRequest.Proxy = null;

                string json = JsonConvert.SerializeObject(request,
                        Newtonsoft.Json.Formatting.None,
                        new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore
                        });

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(JsonConvert.SerializeObject(request,
                        Newtonsoft.Json.Formatting.None,
                        new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore
                        }));
                }

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (StreamReader streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                        return JsonConvert.DeserializeObject<Res>(streamReader.ReadToEnd());
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            
        }

        public static Res RequestGet<Res, Req>(string url)
        {
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.Method = "GET";
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Timeout = 1000 * 60 * 3;
                httpWebRequest.Proxy = null;
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (StreamReader streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    return JsonConvert.DeserializeObject<Res>(streamReader.ReadToEnd());
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public static ResponeModel<CustomerModel> CheckDuplicate(string url,CustomerModel model)
        {
            ResponeModel<CustomerModel> response =  Request<ResponeModel<CustomerModel>, CustomerModel>(
                url+ "api/Core/CheckDuplicateIBAN", model);
            return response;
        }
 
        public static ResponeModel<TransactionModel> CheckExitsForTransfer(string url, TransactionModel model)
        {
            ResponeModel<TransactionModel> response = Request<ResponeModel<TransactionModel>, TransactionModel>(
                url + "api/Core/CheckExistIBANTransfer", model);
            return response;
        }

        
        public static ResponeModel<CustomerModel> CreateAccount(string url, CustomerModel model)
        {
            ResponeModel<CustomerModel> response = Request<ResponeModel<CustomerModel>, CustomerModel>(
                url + "api/Core/CreateAccount", model);
            return response;
        }

        public static ResponeModel<TransactionModel> Transaction(string url, TransactionModel model)
        {
            ResponeModel<TransactionModel> response = Request<ResponeModel<TransactionModel>, TransactionModel>(
                url + "api/Core/Transaction", model);
            return response;
        }



        public static ResponeModel<LoginModel> Login(string url, LoginModel model)
        {
            ResponeModel<LoginModel> response = Request<ResponeModel<LoginModel>, LoginModel>(
                url + "api/Core/Login", model);
            return response;
        }


        public static ResponeModel<List<BankModel>> GetDropdownDataBank(string url)
        {
            ResponeModel<List<BankModel>> response = RequestGet<ResponeModel<List<BankModel>>, BankModel>(
                url + "api/Core/GetBank");
            return response;
        }


        public static ResponeModel<CustomerModel> GetFillIBAN(string url,CustomerModel data)
        {
            ResponeModel<CustomerModel> response = Request<ResponeModel<CustomerModel>, CustomerModel>(
              url + "api/Core/GetIBANNO", data);
            return response;
        }

    }
}
