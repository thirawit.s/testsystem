﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TESTINGSYSTEM.DAO
{
    public class CustomerModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IBAN { get; set; }
        public string BankAccountNo { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string BankName { get; set; }
        public string Id { get; set; }
    }
}
